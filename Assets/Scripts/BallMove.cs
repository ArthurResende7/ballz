using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMove : MonoBehaviour
{
    private Rigidbody rb;

    public Transform cam;

    public float speed;
    public float jumpHeight;
    public int jumpTimesAllowed;
    public float gravityScale;

    private float x;
    private float z;

    private float buttonTime = 0.3f;
    
    private int jumpTimes;
    private float jumpForce;
    private float jumpDuration;
    private bool jumping;

    private Vector3 inPos;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
        jumpTimes = 0;
        jumpForce = Mathf.Sqrt(jumpHeight * -2 * (Physics.gravity.y * gravityScale));
        jumping = false;
        inPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        x = Input.GetAxis("Horizontal") * speed;
        z = Input.GetAxis("Vertical") * speed;

        if (Input.GetKeyDown("left shift"))
        {
            speed *= 2;
        }
        if (Input.GetKeyUp("left shift"))
        {
            speed /= 2;
        }
        
        if (Input.GetKeyDown("space"))
        {
            jumping = true;
            jumpDuration = 0;
            jumpTimes++;
        }

        if (jumping)
        {
            jumpDuration += Time.deltaTime;
        }

        if (Input.GetKeyUp("space") || jumpDuration > buttonTime)
        {
            jumping = false;
        }
    }

    void FixedUpdate()
    {
        Vector3 direction = new Vector3(x, rb.velocity.y, z);
        Vector3 moveDirection = Quaternion.Euler(0.0f, cam.eulerAngles.y, 0.0f) * direction;
        rb.AddForce(moveDirection);

        if (jumping && jumpTimes <= jumpTimesAllowed)
        {
            rb.velocity = new Vector3(rb.velocity.x, jumpForce, rb.velocity.z);
        }
        rb.AddForce(Physics.gravity * gravityScale * rb.mass);

        if (transform.position.y < -100)
        {
            transform.position = inPos;
            rb.velocity = Vector3.zero;
        }
    }
    
    void OnCollisionEnter(Collision other)
    {
        if (other.collider.gameObject.tag == "Floor")
        {
            jumpTimes = 0;
        }
    }
}
