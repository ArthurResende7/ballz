using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class BallSounds : MonoBehaviour
{

    public AudioClip finishSound;
    public AudioClip collectSound;
    public AudioClip jumpSound;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            AudioSource.PlayClipAtPoint(jumpSound, transform.position);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Finish")
        {
            AudioSource.PlayClipAtPoint(finishSound, transform.position);
        }
        if (other.tag == "Star")
        {
            AudioSource.PlayClipAtPoint(collectSound, transform.position);
        }
    }
}
