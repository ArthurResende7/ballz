using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Points : MonoBehaviour
{

    private int points;
    private float counter;
    private int timeBonus;
    private int deathCount;
    private bool deathHappened;

    public float minTimeThreshold;
    public float maxTimeThreshold;

    // Start is called before the first frame update
    void Start()
    {
        points = 0;
        counter = 0.0f;
        timeBonus = 1000;
        deathCount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (counter <= maxTimeThreshold)
        {
            counter += Time.deltaTime;
            if (counter >= minTimeThreshold)
            {
                timeBonus = Mathf.FloorToInt(60000.0f / counter);
            }
        }
        else
        {
            timeBonus = 100;
        }
        if (transform.position.y < -100)
        {
            if (!deathHappened)
            {
                if (points > 0) points -= 100;
                deathCount++;
                deathHappened = true;
            }
        }
        else
        {
            deathHappened = false;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Star")
        {
            points += 100;
        }
        if (other.tag == "Finish")
        {
            points += timeBonus;
            Time.timeScale = 0.0f;
        }
    }
}
